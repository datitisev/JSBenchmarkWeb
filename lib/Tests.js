const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const Collection = require('./Collection');

const Pad = (string, length) => string + ' '.repeat(length - string.length);

class Tests {
  constructor(dir) {
    this.tests = new Collection();
    this.files = [];
    this.dir = dir;
  }

  init(dir) {
    return new Promise((resolve, reject) => {
      const dir = this.dir;
      const cwd = path.resolve(__dirname, `../${dir}`);
      fs.readdir(cwd, (err, files) => {
        if (err) return reject(err);

        if (!files.length) {
          Log.error(`No benchmark tests found`);
          return Log.exit(1);
        }

        files.forEach((f, i) => {
          try {
            const Test = require(`${cwd}/${f}`);
            const testName = f.replace(/\..+/g, '');
            const test = new Test();

            this.tests.set(testName, test);
            this.files.push(f);
          } catch (err) {
            Log.error(`Test | ${f}`, err);
          }

          if (i == files.length - 1) resolve();
        });
      });
    });
  }

  get(name) {
    return this.tests.get(name) || this.tests.filter((test, testName) => testName.toLowerCase() === name.toLowerCase()).first();
  }

  run(name) {
    let test = this.get(name);
    name = test && test.constructor.name;

    if (!test) {
      Log.error(`Benchmark test \`${name}\` doesn't exist`);
      return Log.exit(1);
    }

    Log.info(`${name} | Preparing...`);

    let suite = test.run();

    suite.on('cycle', (e) => {
      if (e.error) return Log.error(this._parseCycleResults(e.target));
      Log.info(this._parseCycleResults(e.target));
    }).on('complete', function () {
      Log.info(`\nFastest is ${chalk.blue(this.filter('fastest').map('name').join(chalk.white(', ')))}`);
    });

    Log.info(`${name} | Running...\n`);

    suite.run({
      async: true,
      minDelay: 2
    });
  }

  _parseCycleResults(bench) {
    const { error, hz, id, stats } = bench;
    const size = stats.sample.length;
    const pm = '\xb1';
    const name = chalk.blue(bench.name || (_.isNaN(id) ? id : '<Test #' + id + '>'));

    if (error) {
      let errorStr = error;
      if (typeof error === 'object') {
        errorStr = error.toString();
      } else if (error instanceof Error) {
        errorStr = error.message;
      }

      return `${Pad(name, 40)}  x  ${chalk.red(errorStr)}`;
    }

    return `${Pad(name, 40)}  ${chalk.cyan('x')}  ${chalk.green(`${this.formatNumber(hz.toFixed(hz < 100 ? 2 : 0))} ops/sec`)} ${chalk.yellow(pm + stats.rme.toFixed(2) + '%')}  ${chalk.grey(`(${size} run${size !== 1 && 's'} sampled)`)}`;
  }

  formatNumber(number) {
    number = number.toString().split('.');
    return `${number[0].replace(/(?=(?:\d{3})+$)(?!\b)/g, ',')}${(number[1] ? '.' + number[1] : '')}`;
  }
}

module.exports = Tests;
