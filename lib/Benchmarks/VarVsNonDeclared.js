'use strict';

const Benchmark = require('benchmark');

class VarVsNonDeclared {
  setup() {
    var foo;
    var bar;
    var baz;
  }

  run() {
    const suite = new Benchmark.Suite();
    const str = 'world';
    const opts = {
      setup: this.setup
    };

    suite.add('declared once', () => {
      var foo = 1;
      var bar = 1;
      var baz = 1;
    });


    suite.add('declared once w/ comma', () => {
      var foo = 1,
      bar = 1,
      baz = 1;
    });

    suite.add('not declared', () => {
      foo = 1;
      bar = 1;
      baz = 1;
    }, opts);

    suite.add('not declared w/ comma', () => {
      foo = 1,
      bar = 1,
      baz = 1;
    }, opts);

    return suite;
  }
}

module.exports = VarVsNonDeclared;
