const Benchmark = require('benchmark');

class ArrayConcatVsSpread {
  run() {
    const suite = new Benchmark.Suite('ArrayConcatVsSpread');
    const opts = {
      setup: () => {
        this.arr1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
        this.arr2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];
        this.arr3 = [];
      },
    };

    suite.add('concat', () => {
      this.arr3 = this.arr1.concat(this.arr2);
    }, opts);

    suite.add('spread', () => {
      this.arr3 = [...this.arr1, ...this.arr2];
    }, opts);

    return suite;
  }
}

module.exports = ArrayConcatVsSpread;
