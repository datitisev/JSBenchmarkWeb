const Benchmark = require('benchmark');

class ArrayLoops {
  constructor() {
    this.arr = [];
  }

  run() {
    const suite = new Benchmark.Suite();

    suite.add('Array#forEach', () => {
      this.arr.forEach(() => {});
    });

    suite.add('for', () => {
      for (let i = 0; i < this.arr.length; i++) { }
    });

    suite.add('for..in', () => {
      for (let prop in this.arr) { }
    });

    suite.add('for..of', () => {
      for (let prop of this.arr) { }
    });

    return suite;
  }
}

module.exports = ArrayLoops;
