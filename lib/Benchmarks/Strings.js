const Benchmark = require('benchmark');

class Strings {
  run() {
    const suite = new Benchmark.Suite();
    const str = 'world';

    suite.add('new String()', () => {
      new String();
    });

    suite.add('Template Literals', () => {
      `hello ${str}`;
    });

    suite.add('Concatenation', () => {
      'hello ' + str;
    });

    return suite;
  }
}

module.exports = Strings;
