const Benchmark = require('benchmark');

class PromiseVsAsyncAwait {
  get description() {
    return 'Using Promise#then vs async/await with try/catch';
  }

  run() {
    const suite = new Benchmark.Suite();

    suite.add('Promise#then && Promise#catch', () => {
      const promiseFunc = () => new Promise(() => {});
      promiseFunc().then(() => {}).catch(console.error);
    });

    suite.add('async/await && try/catch', () => {
      const promiseFunc = () => new Promise(() => {});
      const func = async () => {
        try {
          await promiseFunc();
        } catch (e) {
          console.error(e);
        }
      }
      func();
    });

    return suite;
  }
}

module.exports = PromiseVsAsyncAwait;
