const Benchmark = require('benchmark');

class ArrayFilterVsSplice {
  get description() {
    return 'Test ways to remove index 5,000 from array with 10,000 elements';
  }
  setup() {
    var inputs = [];
    for (var i = 0; i < 10000; i++) {
      inputs.push({ index: i });
    }
  }
  run() {
    const suite = new Benchmark.Suite('ArrayFilterVsSplice');
    const opts = {
      setup: this.setup,
    };

    suite.add('splice - mutable', () => {
      const indexToRemove = 5000;
      inputs.splice(indexToRemove, 1);
      const outputs = inputs;
    }, opts);

    suite.add('filter - immutable', () => {
      const indexToRemove = 5000;
      const outputs = inputs.filter((el, i) => i !== indexToRemove);
    }, opts);

    suite.add('slice - immutable', () => {
      const indexToRemove = 5000;
      const outputs = inputs.slice(0, indexToRemove).concat(inputs.slice(indexToRemove + 1));
    }, opts);

    return suite;
  }
}

module.exports = ArrayFilterVsSplice;
