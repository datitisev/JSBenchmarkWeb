const Benchmark = require('benchmark');

class ArrayMapVsLoop {
  get description() {
    return 'Test Array#Map vs a for loop using Array\'s length';
  }

  setup() {
    this.a = [];
    for (var i = 0; i < 10000; i++) {
      this.a[i] = i;
    }
  }

  run() {
    const suite = new Benchmark.Suite('ArrayMapVsLoop');
    const opts = {
      setup: this.setup,
    };

    suite.add('Array#map', () => {
      this.a.map(e => e++);
    }, opts);

    suite.add('for loop', () => {
      for (var i = 0; i < this.a.length; i++) {
        this.a[i]++;
      }
    }, opts);

    return suite;
  }
}

module.exports = ArrayMapVsLoop;
