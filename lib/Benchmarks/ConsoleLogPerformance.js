const Benchmark = require('benchmark');

class ConsoleLogPerformance {
  get description() {
    return 'console.log vs an empty function returning first argument';
  }

  setup() {
    const emptyFunction = (e) => e;
  }
  run() {
    const suite = new Benchmark.Suite();
    const opts = {
      setup: this.setup,
    }

    suite.add('Empty function', () => {
      emptyFunction('Logging stuff');
    }, opts);

    suite.add('Console#log', () => {
      console.log('Logging stuff');
    }, opts);

    return suite;
  }
}

module.exports = ConsoleLogPerformance;
