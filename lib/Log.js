class Logger {
  constructor(prefix) {
    this.log = require('npmlog');
    this.pe = new (require('pretty-error'))();
    this.prefix = prefix;

    this.info = this.info.bind(this);
    this.warn = this.warn.bind(this);
    this.error = this.error.bind(this);
    this.debug = this.debug.bind(this);
  }
  info(...args) {
    this.log.info(this.prefix, ...args);
  }
  warn(...args) {
    this.log.warn(this.prefix, ...args);
  }
  error(...args) {
    if (args[0] && typeof args[0] === 'object') args[0] = this.pe.render(args[0]);
    this.log.error(this.prefix, ...args);
  }
  debug(...args) {
    this.log.verbose(this.prefix, ...args);
  }
  exit(code = 0) {
    process.exit(code);
  }
}

module.exports = Logger;
