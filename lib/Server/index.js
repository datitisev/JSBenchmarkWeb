module.exports = (Tests) => {
  const express = require('express');
  const path = require('path');
  const cookieParser = require('cookie-parser');
  const bodyParser = require('body-parser');

  const app = express();
  const server = require('http').Server(app);
  const io = require('socket.io')(server);
  const hbs = require('hbs');
  const Swag = require('swag');

  global.io = require('./SocketIO')(io);

  Swag.registerHelpers(hbs);

  hbs.registerHelper('eachInMap', (map, block) => {
    return map.map((value, key) => block.fn({ key, value })).join('');
  });
  hbs.registerHelper('toString', (obj) => {
    return obj.toString ? obj.toString() : String(obj);
  });
  hbs.registerHelper('formatNumber', (num) => {
    return Tests.formatNumber(num.toFixed(0));
  });
  hbs.registerHelper('includes', (object, includes, options, context) => {
  if (arguments.length < 5) throw new Error(`includes: argument length must be 2`);
  if (!object) throw new Error(`includes: \`object\` is "${typeof object === 'object' ? JSON.stringify(object) : object}" (${typeof object}) or not provided`);
  if (!includes) throw new Error(`includes: \`includes\` is "${includes}" (${typeof includes}) or not provided`);
  if (object.includes(includes)) {
    return options.fn(context);
  } else return options.inverse(context);
});

  const routes = require('./Routes');

  app.set('views', path.join(__dirname, '../../views'));
  app.set('view engine', 'hbs');

  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(cookieParser());

  app.use((req, res, next) => {
    req.Tests = Tests;
    next();
  });

  app.use(routes);

  app.use((err, req, res, next) => {
    if (err && err.status != 404) Log.error(err);
    res.render('error', {
      message: err.message,
      error: err,
      path: req.path
    });
  });

  server.listen(8080, () => {
    Log.info('Server | Listening on http://localhost:8080');
  });
}
