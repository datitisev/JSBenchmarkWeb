const app = new require('express').Router();

// === ROUTES ===
const Test = require('./Routes/Test');

app.get('/', (req, res, next) => {
  res.render('index', {
    Tests: req.Tests
  });
});

app.get('/tests/:test', Test);

module.exports = app;
