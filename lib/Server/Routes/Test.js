const funcRegex = /^(?:(.+)?\(\) (?:\=\> )?\{\n {2,4})([\s\S]*)}$/;
const formatNumber = (number) => {
  number = number.toString().split('.');
  return `${number[0].replace(/(?=(?:\d{3})+$)(?!\b)/g, ',')}${(number[1] ? '.' + number[1] : '')}`;
}

const RunTest = (req, res, data, io) => {
  let { suite, test } = data;

  suite.on('cycle', (e) => {
    if (e.error) return next(error);
    let index = data.suite.map((el, i) => {
      if (el.name == e.target.name) return i; else return;
    }).filter(e => !!e);
    data.suite[index] = e.target;
  }).on('complete', function () {
    data.fastest = this.filter('fastest').map('name');
    data.slowest = data.suite.sort((a, b) => a.hz - b.hz).map(e => e.name)[0];
    data.suite = data.suite.map(e => {
      e.fnStr = e.fn.toString().replace(funcRegex, '$2');
      return e;
    });
  });

  res.render('test', data);

  suite.run({
    async: true,
    minDelay: 2
  })
}

const PrepareTest = (data, socket) => {
  let { originalSuite: suite, test, id, name } = data;
  let roomName = `/${name}_${id}`
  const room = io.of(roomName);


  room.on('connection', (socket) => {
    socket.on('run', () => {
      suite.on('cycle', (e) => {
        socket.emit('progress', {
          test: e.target.name,
          hz: formatNumber(e.target.hz.toFixed(0)),
          // fnStr: e.target.fn.toString().replace(funcRegex, '$2'),
          error: e.target.error ? e.target.error.message || e.target.error.toString() : null,
        });
      }).on('complete', function () {
        const fastest = this.filter('fastest').map('name');
        const slowest = data.suite.sort((a, b) => a.hz - b.hz).map(e => e.name)[0];

        socket.emit('complete', {
          fastest, slowest,
        })
      });

      suite.run({
        async: true,
        minDelay: 2,
      });
    });
  });
}

module.exports = (req, res, next) => {
  const Tests = req.Tests;
  const test = Tests.get(req.params.test);
  const io = req.io;

  if (!test) return next('Test not found');

  const name = test.constructor.name;
  const setup = test.setup && test.setup.toString().replace(funcRegex, '$2');
  let suite = test.run();
  const data = {
    suite, test, name, setup,
    id: (Math.random() * 100000).toFixed(0),
    originalSuite: suite,
  };

  data.suite = data.suite.map(e => {
    e.fnStr = e.fn.toString().replace(funcRegex, '$2');
    return e;
  });

  PrepareTest(data, io);

  // if (req.query && req.query.run) return RunTest(req, res, next, data, io);

  res.render('test', data);
}
