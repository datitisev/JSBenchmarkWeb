#! /usr/bin/env node

const args = require('args');
const chalk = require('chalk');
const Tests = new (require('./lib/Tests'))('./lib/Benchmarks');
global.Log = new (require('./lib/Log'))('benchmark');

process.on('uncaughtException', Log.error);
process.on('unhandledRejection', Log.error);

const Pad = (string, length) => string + ' '.repeat(length - string.length);

args.command(['r', 'run'], 'Run a benchmark test', (name, args) => {
  const test = args[0];
  Tests.init().then(() => {
    return Tests.run(test);
  }).catch(Log.error);
});

args.command(['l', 'ls', 'list'], 'List benchmark tests', () => {
  Tests.init().then(() => {
    Log.info(`Found ${Tests.files.length} benchmark tests\n`);
    Tests.files.forEach((testName, i) => {
      let test = Tests.tests.get(testName.replace(/\..+/g, ''));
      Log.info(`• ${chalk.blue(Pad(testName.replace(/\..+/g, ''), 30))}${test && test.description ? '  -  ' + chalk.cyan(test.description) : ''}`);
    });
  }).catch(Log.error);
});

args.command(['s', 'serve', 'server', 'start'], 'Start server', () => {
  Tests.init().then(() => {
    require('./lib/Server')(Tests);
  });
});

args.parse(process.argv, { name: 'benchmark' });

if (args.raw._.length === 0 || !args.isDefined(args.raw._[0], 'commands')) {
  args.showHelp()
}
