# NodeJS Benchmarking Tool

### Start

```sh
node index.js
```

### Use CLI

#### Prepare

With `npm`:

```sh
$ npm link
```

With `yarn`:

```sh
$ yarn link
```

#### Execute

```sh
benchmark
```
